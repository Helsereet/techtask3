﻿namespace FilexEplorerAttempt1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.filePathTextBox = new System.Windows.Forms.TextBox();
            this.openFileButton = new System.Windows.Forms.Button();
            this.fileContentTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.refreshSearchingButton = new System.Windows.Forms.Button();
            this.startStopSearchingButton = new System.Windows.Forms.Button();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.matchedFilePathListBox = new System.Windows.Forms.ListBox();
            this.treeView = new System.Windows.Forms.TreeView();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.treeViewImageList = new System.Windows.Forms.ImageList(this.components);
            this.elapsedTimeTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.findOnlyByTheFileNameCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // filePathTextBox
            // 
            this.filePathTextBox.Enabled = false;
            this.filePathTextBox.Location = new System.Drawing.Point(224, 19);
            this.filePathTextBox.Name = "filePathTextBox";
            this.filePathTextBox.Size = new System.Drawing.Size(252, 20);
            this.filePathTextBox.TabIndex = 0;
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(141, 19);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(67, 20);
            this.openFileButton.TabIndex = 1;
            this.openFileButton.Text = "Open";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
            // 
            // fileContentTextBox
            // 
            this.fileContentTextBox.Location = new System.Drawing.Point(224, 128);
            this.fileContentTextBox.Multiline = true;
            this.fileContentTextBox.Name = "fileContentTextBox";
            this.fileContentTextBox.Size = new System.Drawing.Size(252, 56);
            this.fileContentTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Content in the file:";
            // 
            // refreshSearchingButton
            // 
            this.refreshSearchingButton.Enabled = false;
            this.refreshSearchingButton.Location = new System.Drawing.Point(191, 437);
            this.refreshSearchingButton.Name = "refreshSearchingButton";
            this.refreshSearchingButton.Size = new System.Drawing.Size(82, 25);
            this.refreshSearchingButton.TabIndex = 1;
            this.refreshSearchingButton.Text = "Refresh";
            this.refreshSearchingButton.UseVisualStyleBackColor = true;
            this.refreshSearchingButton.Click += new System.EventHandler(this.RefreshSearchingButton_Click);
            // 
            // startStopSearchingButton
            // 
            this.startStopSearchingButton.Location = new System.Drawing.Point(191, 249);
            this.startStopSearchingButton.Name = "startStopSearchingButton";
            this.startStopSearchingButton.Size = new System.Drawing.Size(82, 25);
            this.startStopSearchingButton.TabIndex = 1;
            this.startStopSearchingButton.Text = "Start/Pause";
            this.startStopSearchingButton.UseVisualStyleBackColor = true;
            this.startStopSearchingButton.Click += new System.EventHandler(this.StartStopButton_Click);
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(224, 67);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(252, 20);
            this.fileNameTextBox.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(151, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "File Name:";
            // 
            // matchedFilePathListBox
            // 
            this.matchedFilePathListBox.FormattingEnabled = true;
            this.matchedFilePathListBox.Location = new System.Drawing.Point(12, 229);
            this.matchedFilePathListBox.Name = "matchedFilePathListBox";
            this.matchedFilePathListBox.Size = new System.Drawing.Size(161, 329);
            this.matchedFilePathListBox.TabIndex = 6;
            // 
            // treeView
            // 
            this.treeView.Location = new System.Drawing.Point(284, 229);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(195, 329);
            this.treeView.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Left files to handle by Content";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(281, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Tree View Representation";
            // 
            // treeViewImageList
            // 
            this.treeViewImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeViewImageList.ImageStream")));
            this.treeViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeViewImageList.Images.SetKeyName(0, "folderImage");
            this.treeViewImageList.Images.SetKeyName(1, "fileImage");
            // 
            // elapsedTimeTextBox
            // 
            this.elapsedTimeTextBox.Enabled = false;
            this.elapsedTimeTextBox.Location = new System.Drawing.Point(191, 343);
            this.elapsedTimeTextBox.Name = "elapsedTimeTextBox";
            this.elapsedTimeTextBox.Size = new System.Drawing.Size(82, 20);
            this.elapsedTimeTextBox.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(188, 327);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Elapsed Time";
            // 
            // findOnlyByTheFileNameCheckBox
            // 
            this.findOnlyByTheFileNameCheckBox.AutoSize = true;
            this.findOnlyByTheFileNameCheckBox.Location = new System.Drawing.Point(224, 93);
            this.findOnlyByTheFileNameCheckBox.Name = "findOnlyByTheFileNameCheckBox";
            this.findOnlyByTheFileNameCheckBox.Size = new System.Drawing.Size(160, 17);
            this.findOnlyByTheFileNameCheckBox.TabIndex = 8;
            this.findOnlyByTheFileNameCheckBox.Text = "Use only name for searching";
            this.findOnlyByTheFileNameCheckBox.UseVisualStyleBackColor = true;
            this.findOnlyByTheFileNameCheckBox.CheckedChanged += new System.EventHandler(this.FindOnlyByTheFileName_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 566);
            this.Controls.Add(this.findOnlyByTheFileNameCheckBox);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.matchedFilePathListBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fileContentTextBox);
            this.Controls.Add(this.startStopSearchingButton);
            this.Controls.Add(this.refreshSearchingButton);
            this.Controls.Add(this.openFileButton);
            this.Controls.Add(this.elapsedTimeTextBox);
            this.Controls.Add(this.fileNameTextBox);
            this.Controls.Add(this.filePathTextBox);
            this.Name = "Form1";
            this.Text = "Tech Task";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button refreshSearchingButton;
        private System.Windows.Forms.Button startStopSearchingButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox matchedFilePathListBox;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ImageList treeViewImageList;
        private System.Windows.Forms.TextBox elapsedTimeTextBox;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox filePathTextBox;
        internal System.Windows.Forms.TextBox fileContentTextBox;
        internal System.Windows.Forms.TextBox fileNameTextBox;
        internal System.Windows.Forms.CheckBox findOnlyByTheFileNameCheckBox;
    }
}

