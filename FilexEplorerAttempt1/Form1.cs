﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.IO;

namespace FilexEplorerAttempt1
{
    public partial class Form1 : Form
    {
        public List<string> FilesPath { get; set; } = new List<string>();

        public System.Timers.Timer Timer { get; set; } = new System.Timers.Timer();

        public TreeNode SelectedDirNode { get; set; }

        public InputData InputData { get; set; }

        public string RootDir { get; set; }

        public bool RunningState { get; set; } = false;

        public bool FirstRunnig { get; set; } = true;

        public bool PermisionDeniedException { get; set; } = false;

        public bool ProblemOccured { get; set; } = false;

        public int LastFoundedFileIndex { get; set; } = 0;

        public bool AtLeastOneFileFounded { get; set; } = false;

        public int TimerSeconds { get; set; } = 0;

        public int TimerMinutes { get; set; } = 0;

        public int TimerHours { get; set; } = 0;

        enum TreeViewImages
        {
            Folder = 0,
            TextFile
        }

        public Form1()
        {
            InitializeComponent();
            InputData = new InputData(this);
            InputData.RestoreDataFromFileJSON();
        }

        private void SwitchRunningState()
        {
            RunningState = !RunningState;
        }

        private void PrepareTimerCountUp()
        {
            Timer.Interval = 1000;
            Timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                TimerSeconds++;
                if (TimerSeconds == 60)
                {
                    TimerMinutes++;
                    TimerSeconds = 0;
                }
                if (TimerMinutes == 60)
                {
                    TimerHours++;
                    TimerMinutes = 0;
                }

                elapsedTimeTextBox.Text = string.Format("{0}:{1}:{2}", TimerHours.ToString().PadLeft(2, '0'),
                                                                        TimerMinutes.ToString().PadLeft(2, '0'),
                                                                        TimerSeconds.ToString().PadLeft(2, '0'));
            }));
        }

        private void PrepareRenderTreeView()
        {
            AddRootDirToTreeView();
            treeView.ImageList = treeViewImageList;
        }

        private void AddFileToTreeNode(string fileName, ref TreeNode currentNode)
        {
            var treeNode = new TreeNode
            {
                Text = fileName,
                Name = fileName,
                ImageIndex = (int)TreeViewImages.TextFile,
                SelectedImageIndex = (int)TreeViewImages.TextFile
            };

            currentNode.Nodes.Add(treeNode);
        }

        private void AddDirsToTreeNode(int startPosition, string[] fileDirs, ref TreeNode currentNode)
        {
            TreeNode treeNode;
            for (int i = startPosition; i < fileDirs.Length - 1; i++)
            {
                treeNode = new TreeNode
                {
                    Text = fileDirs[i],
                    Name = fileDirs[i],
                    ImageIndex = (int)TreeViewImages.Folder,
                    SelectedImageIndex = (int)TreeViewImages.TextFile
                };

                currentNode.Nodes.Add(treeNode);
                currentNode = currentNode.Nodes.Find(fileDirs[i], false)[0];
            }
        }

        private void AddRootDirToTreeView()
        {
            var absolutePath = InputData.FilePathTextBox.Split(Path.DirectorySeparatorChar);
            RootDir = absolutePath[absolutePath.Length - 1];
            SelectedDirNode = new TreeNode
            {
                Text = RootDir,
                Name = RootDir
            };

            treeView.Nodes.Add(SelectedDirNode);
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    InputData.FilePathTextBox = folderBrowserDialog.SelectedPath;
                    filePathTextBox.Text = folderBrowserDialog.SelectedPath.ToString();
                }
            }
        }

        private void StartStopButton_Click(object sender, EventArgs e)
        {
            UserClickedButton();
        }

        private void UserClickedButton()
        {
            if (FirstRunnig && (string.IsNullOrWhiteSpace(fileNameTextBox.Text) ||
                    string.IsNullOrWhiteSpace(filePathTextBox.Text)))
            {
                MessageBox.Show("You must select start folder and file name, for searching", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (FirstRunnig && string.IsNullOrWhiteSpace(fileContentTextBox.Text) && !findOnlyByTheFileNameCheckBox.Checked)
            {
                MessageBox.Show("Enter content for file searching or mark relevant checkbox", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (FirstRunnig)
            {
                PreparationForFirstRunning();
                if (ProblemOccured)
                {
                    ResetTimer();
                    return;
                }
            }

            if (!AtLeastOneFileNameFounded())
            {
                MessageBox.Show("Files with that name not founded. Enter another file name pattern", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ResetTimer();
                FirstRunnig = true;

                return;
            }

            // TODO: handle changing input data
            InputData.StoreDataToFileJSON();
            SwitchRunningState();
            
            RenderInterface();
            if (!AtLeastOneFileFounded)
                MessageBox.Show("Files Not found. Try another file Name/ File Content", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            ResetTimer();
        }

        private bool AtLeastOneFileNameFounded()
        {
            return FilesPath.Count > 0;
        }

        private void PreparationForFirstRunning()
        {
            string pattern;
            // Store input data for trigger if it changed
            InputData.UpdateInputData();
            PrepareTimerCountUp();
            Timer.Start();
            treeView.Nodes.Clear();

            if (!AllowedFileExtension())
            {
                MessageBox.Show("Allowed file name extensions: .txt, .doc", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                ProblemOccured = true;
                return;
            }

            pattern = fileNameTextBox.Text.Trim();
            if (!Path.HasExtension(pattern))
                pattern += ".txt";

            GetFilesPath(pattern);
            if (PermisionDeniedException)
            {
                MessageBox.Show("You havent acces to one of directory. Select another one", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                ProblemOccured = true;
                return;
            }

            BindFoundedFilesPathListBox();
            PrepareRenderTreeView();
            
            refreshSearchingButton.Enabled = true;
            ProblemOccured = false;
            FirstRunnig = false;
            RunningState = false;
            AtLeastOneFileFounded = false;
        }

        private void RenderInterface()
        {
            string[] fileDirs;
            int filesPathLength = FilesPath.Count;

            for (int i = 0; i < filesPathLength; i++)
            {
                // if user click stop
                if (!RunningState)
                    return;

                // if user change at once option
                if (InputData.InputDataChanged())
                {
                    FirstRunnig = true;
                    StartStopButton_Click(this, null);
                    return;
                }

                if (InputData.FindOnlyByTheFileNameCheckBox || 
                    (!InputData.FindOnlyByTheFileNameCheckBox && GetFileContent(FilesPath[0]).IndexOf(InputData.FileContentTextBox) != -1))
                {
                    fileDirs = FilesPath[0].Substring(FilesPath[0].IndexOf(RootDir)).
                        Split(Path.DirectorySeparatorChar).
                        Skip(1).
                        ToArray();
                    AddDirsAndFileToTreeView(fileDirs);

                    AtLeastOneFileFounded = true;
                }


                FilesPath.RemoveAt(0);
                BindFoundedFilesPathListBox();
            }

            //TODO: actions after searching complete: stop, reset timer and ...
            ResetTimer();
            FirstRunnig = true;
        }

        private void AddDirsAndFileToTreeView(string[] fileDirs)
        {
            // fileDirs = [folder, folder, ..., file]
            var currentNode = SelectedDirNode;
            var allNodesFounded = true;
            var startAddNodesFromThatIndex = 0;

            for (int i = 0; i < fileDirs.Length - 1; i++)
            {
                try
                {
                    currentNode = currentNode.Nodes.Find(fileDirs[i], false)[0];
                }
                catch (IndexOutOfRangeException)
                {
                    startAddNodesFromThatIndex = i;
                    allNodesFounded = false;
                    break;
                }
            }

            if (!allNodesFounded)
                AddDirsToTreeNode(startAddNodesFromThatIndex, fileDirs, ref currentNode);
            AddFileToTreeNode(fileDirs[fileDirs.Length - 1], ref currentNode);
        }

        private void BindFoundedFilesPathListBox()
        {
            matchedFilePathListBox.DataSource = null;
            matchedFilePathListBox.DataSource = FilesPath;
        }

        private void ResetTimer()
        {
            Timer.Stop();
            TimerSeconds = TimerMinutes = TimerMinutes = 0;
        }

        private bool AllowedFileExtension()
        {
            string pattern = fileNameTextBox.Text.Trim();

            if (Path.HasExtension(pattern) && 
                (Path.GetExtension(pattern) != ".txt" && Path.GetExtension(pattern) != ".doc"))
                return false;

            return true;
        }

        private void GetFilesPath(string pattern)
        {
            try
            {
                FilesPath = Directory.GetFiles(InputData.FilePathTextBox, pattern, SearchOption.AllDirectories).ToList();
                PermisionDeniedException = false;
            }
            catch (UnauthorizedAccessException)
            {
                PermisionDeniedException = true;
            }
        }

        private string GetFileContent(string filePath)
        {
            // using StreamReader method is shorter than pure FileInfo
            FileInfo fileInfo = new FileInfo(filePath);
            using (FileStream fileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
            using (StreamReader streamReader = new StreamReader(fileStream))
            {
                var result = streamReader.ReadToEnd();
                return result;
            }
        }

        private void RefreshSearchingButton_Click(object sender, EventArgs e)
        {
            FirstRunnig = true;
            UserClickedButton();
        }

        private void FindOnlyByTheFileName_CheckedChanged(object sender, EventArgs e)
        {
            if (findOnlyByTheFileNameCheckBox.Checked)
            {
                fileContentTextBox.Enabled = false;
                return;
            }
            fileContentTextBox.Enabled = true;
        }
    }
}
