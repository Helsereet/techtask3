﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace FilexEplorerAttempt1
{
    [DataContract]
    public class InputData
    {
        [DataMember]
        public string FilePathTextBox { get; set; }

        [DataMember]
        public string FileNameTextBox { get; set; }

        [DataMember]
        public string FileContentTextBox { get; set; }

        [DataMember]
        public bool FindOnlyByTheFileNameCheckBox { get; set; }

        public Form1 Form1 { get; set; }

        public InputData(Form1 form1)
        {
            Form1 = form1;
        }

        public void UpdateInputData()
        {
            FilePathTextBox = Form1.filePathTextBox.Text;
            FileNameTextBox = Form1.fileNameTextBox.Text;
            FileContentTextBox = Form1.fileContentTextBox.Text;
            FindOnlyByTheFileNameCheckBox = Form1.findOnlyByTheFileNameCheckBox.Checked;
        }

        public bool InputDataChanged()
        {
            if (FilePathTextBox != Form1.filePathTextBox.Text ||
                FileNameTextBox != Form1.fileNameTextBox.Text ||
                FileContentTextBox != Form1.fileContentTextBox.Text ||
                FindOnlyByTheFileNameCheckBox != Form1.findOnlyByTheFileNameCheckBox.Checked)
            {
                return true;
            }
            return false;
        }

        internal void StoreDataToFileJSON()
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(InputData));

            using (FileStream fileStream = new FileStream("data.json", FileMode.Create))
            {
                jsonFormatter.WriteObject(fileStream, this);
            }
        }

        internal void RestoreDataFromFileJSON()
        {
            var jsonFormatter = new DataContractJsonSerializer(typeof(InputData));
            InputData dataAsJsonObject;

            try
            {
                using (FileStream fileStream = new FileStream("data.json", FileMode.Open))
                {
                    dataAsJsonObject = (InputData)jsonFormatter.ReadObject(fileStream);

                    FillUserControlsByJsonObject(dataAsJsonObject);
                }
            }
            catch (FileNotFoundException)
            {
                return;
            }

        }

        internal void FillUserControlsByJsonObject(InputData dataAsJsonObject)
        {
            Form1.filePathTextBox.Text = dataAsJsonObject.FilePathTextBox;
            Form1.fileNameTextBox.Text = dataAsJsonObject.FileNameTextBox;
            Form1.fileContentTextBox.Text = dataAsJsonObject.FileContentTextBox;
            Form1.findOnlyByTheFileNameCheckBox.Checked = dataAsJsonObject.FindOnlyByTheFileNameCheckBox;
        }
    }
}
